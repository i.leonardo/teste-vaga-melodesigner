# Laravel v6 - CRUD

Projeto simples usando Docker + mongoDB + FullCalendar

## Iniciar

Se você estiver no Linux, você pode usar `Makefile`, caso esteja usando o Windows você pode seguir as instruções abaixo:

```
# criar os containers
$ make up
> docker-compose up -d

# instalar às dependências
$ make install
> docker-compose exec app composer install

# Verificar se está `UP`
$ make list
> docker-compose ps

# Caso esteja Exit 1 (pois não existia a pasta `vendor`), execute
$ make stop
$ make start
> docker-compose stop
> docker-compose start

# para testar é só abrir o navegador e acessar:
http://localhost:8000/

# caso não funcione, deve verificar qual IP está alocado o Docker
```

**Frontend** está na pasta `frontend`, necessário rodar `yarn install` ou `npm install` para depois `yarn serve` ou `npm run serve` (gere o arquivo `.env` para configurar a rota do backend).

Na pasta [docs](https://gitlab.com/i.leonardo/teste-vaga-troupe-tecnologia/-/tree/main/docs/execution) contém imagens durante a execução da tarefa.

## Licença

[MIT license](https://opensource.org/licenses/MIT).

