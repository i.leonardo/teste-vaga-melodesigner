<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'title' => $faker->firstName,
        'start' => now()->startOfDay(),
        'end' => now()->addDays(1)->endOfDay(),
        'color' => $faker->hexColor,
        'description' => $faker->text,
    ];
});
