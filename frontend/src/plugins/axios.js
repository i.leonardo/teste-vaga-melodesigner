'use strict'

import Vue from 'vue'
import axios from 'axios'

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

const requestLog = (x, title) => {
  const headers = {
    ...x.headers.common,
    ...x.headers[x.method],
    ...x.headers,
  }

  ;['common', 'get', 'post', 'head', 'put', 'patch', 'delete'].forEach(
    (header) => {
      delete headers[header]
    },
  )

  const { url, data, params } = x

  if (process.env.NODE_ENV === 'development') {
    console.info(
      `[${x.method.toUpperCase()}] Request${title ? `<${title}>` : ''}:`,
    )
    console.info({
      url,
      data,
      params,
      headers,
    })
  }
}

const responseLog = (x, title) => {
  if (process.env.NODE_ENV === 'development') {
    console.info(`[${x.status}] Response${title ? `<${title}>` : ''}:`)
    console.info(x.data)
  }
}

let config = {
  baseURL: process.env.VUE_APP_API_URL,
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
}
console.log(process.env)

const _axios = axios.create(config)

_axios.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    requestLog(config)
    return config
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error)
  },
)

// Add a response interceptor
_axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    responseLog(response)
    return response
  },
  function (error) {
    // Do something with response error
    return Promise.reject(error)
  },
)

// eslint-disable-next-line no-unused-vars
Plugin.install = function (Vue, options) {
  Vue.axios = _axios
  window.axios = _axios
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return _axios
      },
    },
    $axios: {
      get() {
        return _axios
      },
    },
  })
}

Vue.use(Plugin)

export default Plugin
