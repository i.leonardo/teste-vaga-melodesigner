# init (and start server)
up:
	docker-compose up -d

# generate vendor
install:
	docker-compose exec app composer install

# check proccess server
list:
	docker-compose ps

# clear cache (app)
clear:
	docker-compose exec app php artisan optimize:clear

test:
	docker-compose exec app php vendor/bin/phpunit

# start server
start:
	docker-compose start

# stop server
stop:
	docker-compose stop

# destroy server
destroy:
	docker-compose down
