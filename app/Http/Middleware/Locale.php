<?php

namespace App\Http\Middleware;

use Closure;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $localization = is_null($request->header('Accept-Language')) ? $request->getLocale() : $request->header('Accept-Language');
        $langFiltered = explode(',', str_replace('-', '_', $localization))[0];

        if (array_key_exists($langFiltered, config('languages'))) {
            app()->setLocale($langFiltered);
            session()->put('locale', $langFiltered);
        } else {
            // This is optional as Laravel will automatically set the fallback language if there is none specified
            app()->setLocale(config('app.fallback_locale'));
            session()->put('locale', config('app.fallback_locale'));
        }

        return $next($request);
    }
}
