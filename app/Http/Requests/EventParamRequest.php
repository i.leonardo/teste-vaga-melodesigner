<?php

namespace App\Http\Requests;

use App\Helpers\Helper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class EventParamRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start' => 'sometimes|' . (Helper::isTimestamp($this->query->get('start')) ? 'numeric' : 'date'),
            'end' => 'required_with:start|' . (Helper::isTimestamp($this->query->get('end')) ? 'numeric' : 'date|after_or_equal:start'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function validated($arr = null): array
    {
        $data = is_null($arr) ? parent::validated() : $arr;

        $dateStart = Arr::get($data, 'start');
        if (!empty($dateStart)) {
            Arr::set($data, 'start', Helper::dateCarbon($dateStart, true));
        }

        $dateEnd = Arr::get($data, 'end');
        if (!empty($dateEnd)) {
            Arr::set($data, 'end', Helper::dateCarbon($dateEnd, true));
        }

        return $data;
    }
}
