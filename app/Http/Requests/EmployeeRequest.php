<?php

namespace App\Http\Requests;

use App\Helpers\Helper;
use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if (!empty($this->color)) {
            $this->merge([
                'color' => Helper::clearHexColor($this->color),
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->routeIs('employees.store')) {
            // request()->isMethod('post')
            $initRule = 'required';
        } else {
            // request()->isMethod('put')
            $initRule = 'sometimes';
        }

        return [
            'name' => "{$initRule}|string|min:3",
            'last_name' => "{$initRule}|string|min:3",
            'color' => "{$initRule}|string|regex:/^#[a-fA-F0-9]{6}$/", // color:complete: #ffffff
        ];
    }
}
