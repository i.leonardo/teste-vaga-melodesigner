<?php

namespace App\Http\Requests;

use App\Helpers\Helper;
use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if (!empty($this->color)) {
            $this->merge([
                'color' => Helper::clearHexColor($this->color),
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->routeIs('events.store')) {
            // request()->isMethod('post')
            $initRule = 'required';
        } else {
            // request()->isMethod('put')
            $initRule = 'sometimes';
        }

        return [
            'title' => "{$initRule}|exists:employees,name",
            'start' => "{$initRule}|date",
            'end' => "{$initRule}|date|after_or_equal:start",
            'color' => "{$initRule}|string|regex:/^#[a-fA-F0-9]{6}$/", // color:complete: #ffffff
            'description' => "sometimes|nullable|string|min:3",
        ];
    }
}
