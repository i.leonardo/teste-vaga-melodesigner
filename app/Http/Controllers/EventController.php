<?php

namespace App\Http\Controllers;

use App\Model\Event;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Requests\EventRequest;
use App\Http\Resources\EventResource;
use App\Http\Requests\EventParamRequest;

class EventController extends Controller
{
    /**
     * GET - fetch_all
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EventParamRequest $request)
    {
        $validated = $request->validated();

        // query
        if (count($validated) === 2) {
            $event = Event::where('start', '>=', $validated['start'])
                ->where('end', '<=', $validated['end'])
                ->orderBy('title', 'DESC')
                ->get();
        } else {
            $event = Event::orderBy('title', 'DESC')->get();
        }

        return EventResource::collection($event);
    }

    /**
     * POST - create
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventRequest $request)
    {
        // query
        $event = Event::create($request->validated());

        return new EventResource($event);
    }

    /**
     * GET - fetch:id
     *
     * @param \App\Model\Event $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return new EventResource($event);
    }

    /**
     * PUT|PATCH - update:id
     *
     * @param \App\Model\Event $event
     * @return \Illuminate\Http\Response
     */
    public function update(EventRequest $request, Event $event)
    {
        // validate
        $validated = $request->validated();
        $input = Helper::dotForm($validated);

        // query
        $event->update($input);

        return new EventResource($event);
    }

    /**
     * DELETE - destroy:id
     *
     * @param \App\Model\Event $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        // query
        $event->delete();

        return response()->json([
            'message' => trans('messages.apiRest.delete'),
        ]);
    }
}
