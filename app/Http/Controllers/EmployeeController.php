<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Http\Requests\EmployeeRequest;
use App\Http\Resources\EmployeeResource;
use App\Model\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * GET - fetch_all
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // query
        $employee = Employee::orderBy('_full_name', 'DESC')->get();

        return EmployeeResource::collection($employee);
    }

    /**
     * POST - create
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        // query
        $employee = Employee::create($request->validated());

        return new EmployeeResource($employee);
    }

    /**
     * GET - fetch:id
     *
     * @param \App\Model\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return new EmployeeResource($employee);
    }

    /**
     * PUT|PATCH - update:id
     *
     * @param \App\Model\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        // validate
        $validated = $request->validated();
        $input = Helper::dotForm($validated);

        // query
        $employee->update($input);

        return new EmployeeResource($employee);
    }

    /**
     * DELETE - destroy:id
     *
     * @param \App\Model\Employee $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        // query
        $employee->delete();

        return response()->json([
            'message' => trans('messages.apiRest.delete'),
        ]);
    }
}
