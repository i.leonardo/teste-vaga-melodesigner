<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);

        return [
            '_id' => $this->id,
            'title' => $this->title,
            'start' => $this->start->format('Y-d-m h:i'),
            'end' => $this->end->format('Y-d-m h:i'),
            'color' => $this->color,
            'description' => empty($this->description) ? null : $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
