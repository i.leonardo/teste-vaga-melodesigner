<?php

namespace App\Model;

use App\Helpers\Helper;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;

class Event extends AbstractModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'start',
        'end',
        'color',
        'description',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start',
        'end',
        'created_at',
        'updated_at',
    ];

    public function setStartAttribute($value)
    {
        $this->attributes['start'] = Helper::dateCarbon($value, true);
    }

    public function setEndAttribute($value)
    {
        $this->attributes['end'] = Helper::dateCarbon($value, true);
    }
}
