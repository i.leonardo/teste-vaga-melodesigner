<?php

namespace App\Model;

class Employee extends AbstractModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'color',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The accessors to append to the model's array form
     * notice that here the attribute name is in snake_case
     *
     * @var array
     */
    protected $appends = ['_full_name'];

    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->last_name}";
    }
}
