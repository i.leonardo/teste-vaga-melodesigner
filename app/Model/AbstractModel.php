<?php

namespace App\Model;

use DateTime;
use Jenssegers\Mongodb\Eloquent\Model;

class AbstractModel extends Model
{
    protected $dateFormat = DateTime::ISO8601;
}
