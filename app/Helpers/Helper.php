<?php

namespace App\Helpers;

use Carbon\Carbon;
use \MongoDB\BSON\UTCDateTime;

class Helper
{
    /**
     * Convert Array in dot notation (only object - Arr::dot())
     */
    public static function dotForm($array, $prepend = '')
    {
        if (empty($array)) {
            return $array;
        }

        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && key($value) !== 0 && key($value) !== null && !empty($value)) {
                $results = array_merge($results, static::dotForm($value, $prepend . $key . '.'));
            } else {
                $results[$prepend . $key] = $value;
            }
        }

        return $results;
    }

    /**
     * Function remove characters
     */
    public static function clearChar($text)
    {
        if (empty($text)) {
            return $text;
        }

        return preg_replace('/[^A-Za-z0-9\ ]/', '', $text);
    }

    /**
     * Function return a clean 6 digit hex number as a string
     */
    public static function clearHexColor($hex)
    {
        if (empty($hex)) {
            return $hex;
        }

        $hex = strtolower($hex);

        //remove the leading "#"
        if (strlen($hex) == 7 || strlen($hex) == 4) {
            $hex = substr($hex, -(strlen($hex) - 1));
        }

        // $hex like "1a7"
        if (preg_match('/^[a-f0-9]{6}$/i', $hex)) {
            return "#{$hex}";
        }

        // $hex like "162a7b"
        else if (preg_match('/^[a-f0-9]{3}$/i', $hex)) {
            return '#' . $hex[0] . $hex[0] . $hex[1] . $hex[1] . $hex[2] . $hex[2];
        }

        return $hex;
    }

    /**
     * Function to check if number is `timestamp`
     */
    public static function isTimestamp($string): bool
    {
        if (empty($string) || !ctype_digit($string)) {
            return false;
        }

        try {
            $filtered = rtrim(strval($string), '0');
            Carbon::createFromTimestamp($filtered);
        } catch (Throwable $th) {
            return false;
        }

        return true;
    }

    /**
     * Function format Date
     */
    public static function dateCarbon($date, $isMongo = false)
    {
        if (empty($date)) {
            return $date;
        }

        $carbon = null;
        try {
            if ($date instanceof UTCDatetime) {
                $carbon = Carbon::createFromTimestamp($date->toDateTime()->getTimestamp());
            } else if (self::isTimestamp($date)) {
                $filtered = rtrim($date, '0');
                $carbon = Carbon::createFromTimestamp($filtered);
            } else {
                $carbon = Carbon::parse($date);
            }
        } catch (\Carbon\Exceptions\InvalidFormatException $e) {
            return null;
        }

        if ($isMongo) {
            return new UTCDateTime($carbon);
        }
        return $carbon;
    }
}
