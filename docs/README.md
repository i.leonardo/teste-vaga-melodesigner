Uma página onde posso adicionar pessoas. 
Nela vou poder adicionar uma pessoa com nome e sobrenome e escolher uma cor. 
Opção de editar e deletar!

Você também vai ter que criar um ToDo com laravel. 

Lá vou poder adicionar tarefas e escolher as pessoas que vão participar. Essas pessoas vem da lista de cima. 
também vou poder escolher data de fim, hora, status: (Sent, In Progress, Done)  colocar anotações extras e escolher se essas ToDos devem se repetir: semanalmente, a cada duas semanas, mensalmente ou anualmente.

Quero que crie um calendário usando o FullCalendar.

Nesse calendário vão aparecer as tarefas do ToDo Também vou poder ver quem são as pessoas que participam dessa tarefa.  Também vai ter um botão pra visualizar essa tarefa. Ela pode aparecer como popup ou você pode redirecionar a página.   Eu também vou poder criar novos eventos nesse calendário. Também vou poder adicionar novas pessoas como um multi select  No calendário, ao lado esquerdo, deve haver uma lista com todas as pessoas criadas. Essa lista deve ser dinâmica. Toda vez que eu adicionar uma nova pessoa, ela vai aparecer automaticamente lá. 

Quando eu clicar num nome de uma pessoa, deverão ser filtradas todas as tarefas dessa pessoa. 

Isso vai funcionar pra todas as pessoas da lista. Cada pessoa com sua cor. A cor das tarefas devem corresponder com a cor da pessoa.  
Toda essa aplicação deve ser traduzida pra português e inglês. Vou ter que ter uma opção no menu pra eu trocar o idioma.
